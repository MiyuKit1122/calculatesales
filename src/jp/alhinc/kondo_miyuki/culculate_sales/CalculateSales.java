package jp.alhinc.kondo_miyuki.culculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		HashMap<String, String> branchNameMap = new HashMap<String, String>();
		HashMap<String, Long> moneyMap = new HashMap<String, Long>();


		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		//ストリング型で、ディレクトリを指すdirを宣言する。
		String dir = args[0];

		/*
		 * ファイル読み込み
		 */
		
		if(!lstFileReader(dir, "branch.lst", branchNameMap, moneyMap)){
			return;
		}

		/*
		 *	集計処理
		 */
		// (集計処理準備段階)支店定義ファイル参照し、
		File file = new File(args[0]);
		// listFilesメソッドを使用して一覧を取得する。
		File[] list = file.listFiles();

		FileReader fr;
		BufferedReader br = null;
		ArrayList<File> saleFile = new ArrayList<File>();

		// Integer（支店定義ファイルの末尾の数字）を、配列に入れる。
		ArrayList<Integer> defFileNumber = new ArrayList<>();

		//集計処理を実際に始める
		for (int i = 0; i < list.length; i++) {
			if (list[i].getName().toString().matches("^\\d{8}.rcd$") && list[i].isFile()) {

				// saleFileというリストに8桁の数字で.rcdがつくもの(売上)を入れる
				saleFile.add(list[i]);

				// fileNameという変数をつくり、ファイルの名前を読み込んで点で分ける
				String[] fileNumber = list[i].getName().split("\\.");

				// 変換した変数の入れ物をつくってから変換、スプリットで分けた[0]と[1]の要素のどちらを変換するか示す
				int fileNum = Integer.parseInt(fileNumber[0]);

				// リストにファイル名を入れて前のファイル名と比較するために、まずはリストを作る
				// リストに入れないと、forループで繰り返すたびに上書きされて比較できない
				defFileNumber.add(fileNum);
			}
		}

		// 連番チェック
		Collections.sort(defFileNumber);
		for (int i = 0; i < defFileNumber.size() - 1; i++) {
			int after = defFileNumber.get(i + 1);
			int before = defFileNumber.get(i);
			// System.out.println(before);
			// System.out.println(after);
			// System.out.println(before - after);

			if (after - before != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		for (int i = 0; i < saleFile.size(); i++) {
			try{
				ArrayList<String> saleSum = new ArrayList<String>();
					fr = new FileReader(saleFile.get(i));
					br = new BufferedReader(fr);
					String readFile;
				while ((readFile = br.readLine()) != null) {
					saleSum.add(readFile);
				}

				//売上ファイルが2行以外だったらエラーとしてはじく
				if(saleSum.size() != 2){
					System.out.println(saleFile.get(i).getName() + "のフォーマットが不正です");
					return;
				}

				//売上ファイルの支店コードが支店定義ファイルに存在しない場合
				if (!moneyMap.containsKey(saleSum.get(0))) {
				// System.out.println(branchNameMap.get(saleFile.get(0)));
					System.out.println(saleFile.get(i).getName() + "の支店コードが不正です");
					return;
				}

				//売上ファイルに数字以外が含まれていたらエラーとしてはじく
				if(!saleSum.get(1).matches("^\\d+$")){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				long allSumL = moneyMap.get(saleSum.get(0)) + Long.parseLong(saleSum.get(1));
				String allSum = Long.toString(allSumL);
				if (allSum.length() >= 11) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				moneyMap.put(saleSum.get(0), allSumL);
			} catch (IOException e) {
				return;
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						return;
					}
				}
			}
		}

		/*
		 * ファイル書き込み
		 */
		if(!salesFileWriter(dir,"branch.out", branchNameMap, moneyMap)){
			return;
		}
	}

	/*
	 * ファイル読み込みメソッド
	 */
	public static boolean lstFileReader(String dir,String fileName, HashMap<String, String> defMap, HashMap<String, Long> moneyMap){
		// public static filereading(){
		BufferedReader br = null;
		//System.out.println("ファイルリーディングメソッドが呼ばれました");
		try {
			File file = new File(dir,fileName);
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			// fileの中身を読み込み始める
			String line;
			while ((line = br.readLine()) != null) {
				String[] items = line.split(",");

				int defFileLength = items.length;

				// 支店定義ファイルの要素数が2個以外になった場合
				if (defFileLength != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				// 支店定義ファイル中の支店コードが3桁の数字以外の場合(\Dで、半角数字以外)
				if (!items[0].matches("^[0-9]{3}$")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}

				// 元branchNameMap、各店舗の支店番号(キー)と売り上げ(バリュー、値)を設定
				defMap.put(items[0], items[1]);
				//「0L(ゼロエル)」のLは、ロング型を示す
				moneyMap.put(items[0], 0L);
			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					return false;
				}
			}
		}
		return true;
	}

	/*
	 * ファイル書き込みメソッド
	 */
	public static boolean salesFileWriter(String dir, String fileName, HashMap<String, String> branchNameMap, HashMap<String, Long> moneyMap){
		BufferedWriter bw = null;
		//System.out.println("ファイルライティングメソッドが呼ばれました");
		try {
			File bufferedwriter = new File(dir,fileName);
			FileWriter fw = new FileWriter(bufferedwriter);
			bw = new BufferedWriter(fw);
			for (Map.Entry<String, String> entry : branchNameMap.entrySet()) {
				bw.write(entry.getKey() + "," + entry.getValue() + "," + moneyMap.get(entry.getKey()));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					return false;
				}
			}
		}
		return true;
	}
}


