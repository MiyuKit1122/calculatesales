package jp.alhinc.kondo_miyuki.culculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {

	/**
	 * @param args
	 */
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		HashMap<String, String> branchNameMap = new HashMap<String, String>();
		HashMap<String, Long> Moneymap = new HashMap<String, Long>();


		System.out.println("ここにあるファイルを開きます。 => " + args[0]);

		BufferedReader br = null;
		try {
			File file = new File(args[0], "branch.lst");
			FileReader fr = new FileReader(file);

			if (!file.exists()) {
	            System.out.println("支店定義ファイルが存在しません。");
	             return;
	        }
			file.getName().toString()

				System.out.println("売り上げファイルが連番になっていません。");


			br = new BufferedReader(fr);

			String line;
			while ((line = br.readLine()) != null) {
				System.out.println(line);
				String[] items = line.split(",");
				System.out.println(items[0]);
				System.out.println(items[1]);

				if (!items[0].matches("[0-9]{3}")) {
					System.out.println("エラー");
					return;
				}

				branchNameMap.put(items[0], items[1]);
				Moneymap.put(items[0],0L);							//各店舗の支店番号(キー)と売り上げ(値)
																	//を関連づける。ロング型の1行目という意味で
																	//「0L」(ゼロ･エル)と記す。
			}

			System.out.println(branchNameMap.entrySet());

		}catch (IOException e) {
			e.printStackTrace();
			System.out.println("エラーが発生しました。");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("クローズできませんでした。");
				}
			}
		}

		File file = new File(args[0]);								 // ファイル参照していることを示す。


		File[] list = file.listFiles();								// listFilesメソッドを使用して一覧を取得する。

		System.out.println(list.length);
			ArrayList<String> salesum = null;
		for (int i = 0; i < list.length; i++) {
			if (list[i].getName().toString().matches("^\\d{8}.rcd$")) {
				try {
					salesum = new ArrayList<String>();
					FileReader fr = new FileReader(list[i]);
					br = new BufferedReader(fr);
					String xfile;
					while ((xfile = br.readLine()) != null) {
						salesum.add(xfile);
						System.out.println(xfile);
					}

				} catch (IOException e) {
					e.printStackTrace();
					System.out.println("エラーが発生しました。");
				} finally {
					if (br != null) {
						try {
							br.close();
						} catch (IOException e) {
							System.out.println("クローズできませんでした。");
						}
					}
				}
			}
			salesum.get(0);
			long allsum = Moneymap.get(salesum.get(0)) + Long.parseLong(salesum.get(1));
				Moneymap.put(salesum.get(0) , allsum);
				BufferedWriter bo = null;
				try{
					  File bufferedwriter = new File(args[0],"branch.out");
					  FileWriter fw = new FileWriter(bufferedwriter);
					  bo = new BufferedWriter(fw);
					  for(Map.Entry<String, String> entry : branchNameMap.entrySet()) {

						  bo.write(entry.getKey() + "," + entry.getValue() + "," + Moneymap.get(salesum.get(0)) + "\n");
					  }
					  bo.close();
				}catch(IOException e){
					e.printStackTrace();
					  System.out.println("エラーが発生しました。");
				}

		}
		System.out.println(Moneymap.entrySet());

}
}